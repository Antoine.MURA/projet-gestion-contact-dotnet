﻿namespace ContactManager
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            BT_up = new Button();
            TBox_rep_nom = new TextBox();
            TBox_contact_nom = new TextBox();
            TBox_contact_prenom = new TextBox();
            BT_mkdir = new Button();
            BT_touch = new Button();
            TBox_tree = new RichTextBox();
            Label_static_tree = new Label();
            TBox_debug = new TextBox();
            CBox_tri = new ComboBox();
            BT_tri_invert = new Button();
            Panel_Files = new Panel();
            trackBar1 = new TrackBar();
            BT_rm = new Button();
            BT_rmdir = new Button();
            BT_update = new Button();
            TBox_setnom = new TextBox();
            Label_static_Nom = new Label();
            TBox_setprenom = new TextBox();
            Label_static_Type = new Label();
            BT_serialize = new Button();
            TBox_serialize_path = new TextBox();
            BT_open = new Button();
            BT_copy = new Button();
            BT_cut = new Button();
            BT_paste = new Button();
            label1 = new Label();
            label2 = new Label();
            TBox_setposte = new TextBox();
            TBox_setsurnom = new TextBox();
            TBox_settel = new TextBox();
            label3 = new Label();
            BT_undo = new Button();
            BT_redo = new Button();
            TBox_EncryptKey = new TextBox();
            ((System.ComponentModel.ISupportInitialize)trackBar1).BeginInit();
            SuspendLayout();
            // 
            // BT_up
            // 
            BT_up.Enabled = false;
            BT_up.Location = new Point(12, 7);
            BT_up.Name = "BT_up";
            BT_up.Size = new Size(30, 23);
            BT_up.TabIndex = 1;
            BT_up.Text = "up";
            BT_up.UseVisualStyleBackColor = true;
            BT_up.Click += BT_up_Click;
            // 
            // TBox_rep_nom
            // 
            TBox_rep_nom.Location = new Point(378, 6);
            TBox_rep_nom.Name = "TBox_rep_nom";
            TBox_rep_nom.PlaceholderText = "Nom du dossier";
            TBox_rep_nom.Size = new Size(254, 23);
            TBox_rep_nom.TabIndex = 7;
            TBox_rep_nom.KeyDown += TBox_mkdir_name_KeyDown;
            // 
            // TBox_contact_nom
            // 
            TBox_contact_nom.Location = new Point(500, 34);
            TBox_contact_nom.Name = "TBox_contact_nom";
            TBox_contact_nom.PlaceholderText = "Nom du contact";
            TBox_contact_nom.Size = new Size(132, 23);
            TBox_contact_nom.TabIndex = 10;
            TBox_contact_nom.KeyDown += TBox_touch_name_KeyDown;
            // 
            // TBox_contact_prenom
            // 
            TBox_contact_prenom.Location = new Point(378, 35);
            TBox_contact_prenom.Name = "TBox_contact_prenom";
            TBox_contact_prenom.PlaceholderText = "Prenom du contact";
            TBox_contact_prenom.Size = new Size(116, 23);
            TBox_contact_prenom.TabIndex = 9;
            TBox_contact_prenom.KeyDown += TBox_touch_extension_KeyDown;
            // 
            // BT_mkdir
            // 
            BT_mkdir.Location = new Point(639, 7);
            BT_mkdir.Name = "BT_mkdir";
            BT_mkdir.Size = new Size(137, 23);
            BT_mkdir.TabIndex = 8;
            BT_mkdir.Text = "Creer dossier";
            BT_mkdir.UseVisualStyleBackColor = true;
            BT_mkdir.Click += BT_mkdir_Click;
            // 
            // BT_touch
            // 
            BT_touch.Location = new Point(638, 36);
            BT_touch.Name = "BT_touch";
            BT_touch.Size = new Size(137, 23);
            BT_touch.TabIndex = 11;
            BT_touch.Text = "Creer contact";
            BT_touch.UseVisualStyleBackColor = true;
            BT_touch.Click += BT_touch_Click;
            // 
            // TBox_tree
            // 
            TBox_tree.Location = new Point(782, 122);
            TBox_tree.Name = "TBox_tree";
            TBox_tree.ReadOnly = true;
            TBox_tree.Size = new Size(445, 218);
            TBox_tree.TabIndex = 33;
            TBox_tree.Text = "";
            // 
            // Label_static_tree
            // 
            Label_static_tree.AutoSize = true;
            Label_static_tree.Location = new Point(678, 122);
            Label_static_tree.Name = "Label_static_tree";
            Label_static_tree.Size = new Size(0, 15);
            Label_static_tree.TabIndex = 12;
            // 
            // TBox_debug
            // 
            TBox_debug.Location = new Point(13, 93);
            TBox_debug.Name = "TBox_debug";
            TBox_debug.Size = new Size(762, 23);
            TBox_debug.TabIndex = 16;
            // 
            // CBox_tri
            // 
            CBox_tri.FormattingEnabled = true;
            CBox_tri.Location = new Point(120, 7);
            CBox_tri.Name = "CBox_tri";
            CBox_tri.Size = new Size(85, 23);
            CBox_tri.TabIndex = 5;
            CBox_tri.SelectedIndexChanged += CBox_tri_SelectedIndexChanged;
            // 
            // BT_tri_invert
            // 
            BT_tri_invert.Location = new Point(12, 36);
            BT_tri_invert.Name = "BT_tri_invert";
            BT_tri_invert.Size = new Size(30, 23);
            BT_tri_invert.TabIndex = 4;
            BT_tri_invert.Text = "↑";
            BT_tri_invert.UseVisualStyleBackColor = true;
            BT_tri_invert.Click += BT_tri_invert_Click;
            // 
            // Panel_Files
            // 
            Panel_Files.Location = new Point(12, 123);
            Panel_Files.Name = "Panel_Files";
            Panel_Files.Size = new Size(764, 218);
            Panel_Files.TabIndex = 32;
            // 
            // trackBar1
            // 
            trackBar1.Location = new Point(12, 345);
            trackBar1.Maximum = 200;
            trackBar1.Minimum = 50;
            trackBar1.Name = "trackBar1";
            trackBar1.Size = new Size(153, 45);
            trackBar1.TabIndex = 34;
            trackBar1.Value = 100;
            trackBar1.Scroll += trackBar1_Scroll;
            // 
            // BT_rm
            // 
            BT_rm.Enabled = false;
            BT_rm.Location = new Point(1040, 93);
            BT_rm.Name = "BT_rm";
            BT_rm.Size = new Size(80, 23);
            BT_rm.TabIndex = 31;
            BT_rm.Text = "Supprimer";
            BT_rm.UseVisualStyleBackColor = true;
            BT_rm.Click += BT_rm_Click;
            // 
            // BT_rmdir
            // 
            BT_rmdir.Location = new Point(48, 36);
            BT_rmdir.Name = "BT_rmdir";
            BT_rmdir.Size = new Size(157, 23);
            BT_rmdir.TabIndex = 12;
            BT_rmdir.Text = "Supprimer dossier";
            BT_rmdir.UseVisualStyleBackColor = true;
            BT_rmdir.Click += BT_rmdir_Click;
            // 
            // BT_update
            // 
            BT_update.Location = new Point(1126, 94);
            BT_update.Name = "BT_update";
            BT_update.Size = new Size(101, 23);
            BT_update.TabIndex = 30;
            BT_update.Text = "Mettre à jour";
            BT_update.UseVisualStyleBackColor = true;
            BT_update.Click += BT_update_Click;
            // 
            // TBox_setnom
            // 
            TBox_setnom.Enabled = false;
            TBox_setnom.Location = new Point(849, 8);
            TBox_setnom.Name = "TBox_setnom";
            TBox_setnom.PlaceholderText = "Nom";
            TBox_setnom.Size = new Size(145, 23);
            TBox_setnom.TabIndex = 18;
            TBox_setnom.KeyDown += TBox_setnom_KeyDown;
            // 
            // Label_static_Nom
            // 
            Label_static_Nom.AutoSize = true;
            Label_static_Nom.Location = new Point(782, 13);
            Label_static_Nom.Name = "Label_static_Nom";
            Label_static_Nom.Size = new Size(34, 15);
            Label_static_Nom.TabIndex = 17;
            Label_static_Nom.Text = "Nom";
            // 
            // TBox_setprenom
            // 
            TBox_setprenom.Enabled = false;
            TBox_setprenom.Location = new Point(1055, 10);
            TBox_setprenom.Name = "TBox_setprenom";
            TBox_setprenom.PlaceholderText = "Prenom";
            TBox_setprenom.Size = new Size(172, 23);
            TBox_setprenom.TabIndex = 20;
            TBox_setprenom.KeyDown += TBox_setprenom_KeyDown;
            // 
            // Label_static_Type
            // 
            Label_static_Type.AutoSize = true;
            Label_static_Type.Location = new Point(1000, 13);
            Label_static_Type.Name = "Label_static_Type";
            Label_static_Type.Size = new Size(49, 15);
            Label_static_Type.TabIndex = 19;
            Label_static_Type.Text = "Prenom";
            // 
            // BT_serialize
            // 
            BT_serialize.Location = new Point(1131, 375);
            BT_serialize.Name = "BT_serialize";
            BT_serialize.Size = new Size(96, 23);
            BT_serialize.TabIndex = 15;
            BT_serialize.Text = "Enregistrer";
            BT_serialize.UseVisualStyleBackColor = true;
            BT_serialize.Click += BT_serialize_Click;
            // 
            // TBox_serialize_path
            // 
            TBox_serialize_path.Location = new Point(782, 346);
            TBox_serialize_path.Name = "TBox_serialize_path";
            TBox_serialize_path.PlaceholderText = "Chemin du fichier XML";
            TBox_serialize_path.Size = new Size(343, 23);
            TBox_serialize_path.TabIndex = 13;
            TBox_serialize_path.KeyDown += TBox_serialize_path_KeyDown;
            // 
            // BT_open
            // 
            BT_open.Location = new Point(1131, 345);
            BT_open.Name = "BT_open";
            BT_open.Size = new Size(96, 23);
            BT_open.TabIndex = 14;
            BT_open.Text = "Ouvrir";
            BT_open.UseVisualStyleBackColor = true;
            BT_open.Click += BT_open_Click;
            // 
            // BT_copy
            // 
            BT_copy.Location = new Point(782, 93);
            BT_copy.Name = "BT_copy";
            BT_copy.Size = new Size(80, 23);
            BT_copy.TabIndex = 27;
            BT_copy.Text = "Copier";
            BT_copy.UseVisualStyleBackColor = true;
            BT_copy.Click += BT_copy_Click;
            // 
            // BT_cut
            // 
            BT_cut.Location = new Point(954, 93);
            BT_cut.Name = "BT_cut";
            BT_cut.Size = new Size(80, 23);
            BT_cut.TabIndex = 29;
            BT_cut.Text = "Couper";
            BT_cut.UseVisualStyleBackColor = true;
            BT_cut.Click += BT_cut_Click;
            // 
            // BT_paste
            // 
            BT_paste.Enabled = false;
            BT_paste.Location = new Point(868, 93);
            BT_paste.Name = "BT_paste";
            BT_paste.Size = new Size(80, 23);
            BT_paste.TabIndex = 28;
            BT_paste.Text = "Coller";
            BT_paste.UseVisualStyleBackColor = true;
            BT_paste.Click += BT_paste_Click;
            // 
            // label1
            // 
            label1.AutoSize = true;
            label1.Location = new Point(1000, 42);
            label1.Name = "label1";
            label1.Size = new Size(36, 15);
            label1.TabIndex = 23;
            label1.Text = "Poste";
            // 
            // label2
            // 
            label2.AutoSize = true;
            label2.Location = new Point(782, 42);
            label2.Name = "label2";
            label2.Size = new Size(49, 15);
            label2.TabIndex = 21;
            label2.Text = "Surnom";
            // 
            // TBox_setposte
            // 
            TBox_setposte.Enabled = false;
            TBox_setposte.Location = new Point(1055, 39);
            TBox_setposte.Name = "TBox_setposte";
            TBox_setposte.PlaceholderText = "Poste";
            TBox_setposte.Size = new Size(172, 23);
            TBox_setposte.TabIndex = 24;
            TBox_setposte.KeyDown += TBox_setposte_KeyDown;
            // 
            // TBox_setsurnom
            // 
            TBox_setsurnom.Enabled = false;
            TBox_setsurnom.Location = new Point(849, 39);
            TBox_setsurnom.Name = "TBox_setsurnom";
            TBox_setsurnom.PlaceholderText = "Surnom";
            TBox_setsurnom.Size = new Size(145, 23);
            TBox_setsurnom.TabIndex = 22;
            TBox_setsurnom.KeyDown += TBox_setsurnom_KeyDown;
            // 
            // TBox_settel
            // 
            TBox_settel.Enabled = false;
            TBox_settel.Location = new Point(849, 68);
            TBox_settel.Name = "TBox_settel";
            TBox_settel.PlaceholderText = "Téléphone";
            TBox_settel.Size = new Size(378, 23);
            TBox_settel.TabIndex = 26;
            TBox_settel.KeyDown += TBox_settel_KeyDown;
            // 
            // label3
            // 
            label3.AutoSize = true;
            label3.Location = new Point(782, 71);
            label3.Name = "label3";
            label3.Size = new Size(61, 15);
            label3.TabIndex = 25;
            label3.Text = "Téléphone";
            // 
            // BT_undo
            // 
            BT_undo.Enabled = false;
            BT_undo.Location = new Point(48, 7);
            BT_undo.Name = "BT_undo";
            BT_undo.Size = new Size(30, 23);
            BT_undo.TabIndex = 2;
            BT_undo.Text = "⭠";
            BT_undo.UseVisualStyleBackColor = true;
            BT_undo.Click += BT_undo_Click;
            // 
            // BT_redo
            // 
            BT_redo.Enabled = false;
            BT_redo.Location = new Point(84, 7);
            BT_redo.Name = "BT_redo";
            BT_redo.Size = new Size(30, 23);
            BT_redo.TabIndex = 3;
            BT_redo.Text = "⭢";
            BT_redo.UseVisualStyleBackColor = true;
            BT_redo.Click += BT_redo_Click;
            // 
            // TBox_EncryptKey
            // 
            TBox_EncryptKey.Location = new Point(782, 375);
            TBox_EncryptKey.Name = "TBox_EncryptKey";
            TBox_EncryptKey.PlaceholderText = "Clé de chiffrement (16, 24 ou 32 caractères)";
            TBox_EncryptKey.Size = new Size(343, 23);
            TBox_EncryptKey.TabIndex = 35;
            TBox_EncryptKey.Text = "CleAES256Bits1234567890123456789";
            // 
            // Form1
            // 
            AutoScaleDimensions = new SizeF(7F, 15F);
            AutoScaleMode = AutoScaleMode.Font;
            ClientSize = new Size(1242, 419);
            Controls.Add(TBox_EncryptKey);
            Controls.Add(BT_redo);
            Controls.Add(BT_undo);
            Controls.Add(TBox_contact_nom);
            Controls.Add(TBox_settel);
            Controls.Add(label3);
            Controls.Add(TBox_setposte);
            Controls.Add(TBox_setsurnom);
            Controls.Add(label1);
            Controls.Add(label2);
            Controls.Add(BT_paste);
            Controls.Add(BT_cut);
            Controls.Add(BT_copy);
            Controls.Add(BT_open);
            Controls.Add(TBox_serialize_path);
            Controls.Add(BT_serialize);
            Controls.Add(Label_static_Type);
            Controls.Add(TBox_setprenom);
            Controls.Add(Label_static_Nom);
            Controls.Add(TBox_setnom);
            Controls.Add(BT_update);
            Controls.Add(BT_rmdir);
            Controls.Add(BT_rm);
            Controls.Add(trackBar1);
            Controls.Add(Panel_Files);
            Controls.Add(BT_tri_invert);
            Controls.Add(CBox_tri);
            Controls.Add(TBox_debug);
            Controls.Add(Label_static_tree);
            Controls.Add(TBox_tree);
            Controls.Add(BT_touch);
            Controls.Add(BT_mkdir);
            Controls.Add(TBox_contact_prenom);
            Controls.Add(TBox_rep_nom);
            Controls.Add(BT_up);
            Name = "Form1";
            Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)trackBar1).EndInit();
            ResumeLayout(false);
            PerformLayout();
        }

        #endregion
        private Button BT_up;
        private TextBox TBox_rep_nom;
        private TextBox TBox_contact_nom;
        private TextBox TBox_contact_prenom;
        private Button BT_mkdir;
        private Button BT_touch;
        private RichTextBox TBox_tree;
        private Label Label_static_tree;
        private TextBox TBox_debug;
        private ComboBox CBox_tri;
        private Button BT_tri_invert;
        private Panel Panel_Files;
        private TrackBar trackBar1;
        private Button BT_rm;
        private Button BT_rmdir;
        private Button BT_update;
        private TextBox TBox_setnom;
        private Label Label_static_Nom;
        private TextBox TBox_setprenom;
        private TextBox TBox_setsurnom;
        private TextBox TBox_setposte;
        private Label Label_static_Type;
        private Button BT_serialize;
        private TextBox TBox_serialize_path;
        private Button BT_open;
        private Button BT_copy;
        private Button BT_cut;
        private Button BT_paste;
        private Label label1;
        private Label label2;
        private TextBox TBox_settel;
        private Label label3;
        private Button BT_undo;
        private Button BT_redo;
        private TextBox TBox_EncryptKey;
    }
}
