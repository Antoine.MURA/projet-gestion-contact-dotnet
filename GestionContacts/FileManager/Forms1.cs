using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;
using System.Xml.Linq;
using System.Xml.Serialization;
using ContactManager.Tools;
using Microsoft.VisualBasic.ApplicationServices;
using static System.Runtime.InteropServices.JavaScript.JSType;

namespace ContactManager
{
    public partial class Form1 : Form
    {
        private Manager g = new Manager();
        private FlowLayoutPanel flowLayoutPanel = new FlowLayoutPanel();
        private List<Element> sortedElements = new List<Element>();
        private Contact? selectedContact = null;
        private Contact? contactToPaste = null;
        private bool deleteAfterPaste = false;
        private int btSize = 0;
        private List<Repository> historique = new List<Repository>();
        int positionHistorique = 0;
        int sizeHistorique = 10;

        public Form1()
        {
            InitializeComponent();
            btSize = trackBar1.Value;
            flowLayoutPanel.AutoScroll = true;
            flowLayoutPanel.Dock = DockStyle.Fill;

            //Initialisation des tris par d�faut
            string[] tris = { "nom", "prenom", "date" };
            foreach (string tr in tris)
            {
                CBox_tri.Items.Add(tr);
            }
            CBox_tri.SelectedItem = tris[0];

            Historique_Add(g.actuel);
            Update_RedoUndo();
            Load_Contacts();
        }

        private void Historique_Add(Repository rep)
        {
            historique.Add(g.actuel);
            if(historique.Count > sizeHistorique)
            {
                historique.RemoveAt(0);
            }
        }

        private byte[] Encrypt(string plainText, string key)
        {
            //Chiffre les donn�es mises en param�tre avec une cl� de chiffrement
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(key);
                aesAlg.IV = new byte[aesAlg.BlockSize / 8];

                ICryptoTransform encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        byte[] plainBytes = Encoding.UTF8.GetBytes(plainText);
                        csEncrypt.Write(plainBytes, 0, plainBytes.Length);
                        csEncrypt.FlushFinalBlock();  // Add this line
                    }
                    return msEncrypt.ToArray();
                }
            }
        }

        private string Decrypt(byte[] cipherText, string key)
        {
            //D�chiffre les donn�es mises en param�tre avec une cl� de chiffrement
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = Encoding.UTF8.GetBytes(key);
                aesAlg.IV = new byte[aesAlg.BlockSize / 8];

                ICryptoTransform decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

                using (MemoryStream msDecrypt = new MemoryStream(cipherText))
                {
                    using (CryptoStream csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    {
                        using (StreamReader srDecrypt = new StreamReader(csDecrypt))
                        {
                            return srDecrypt.ReadToEnd();
                        }
                    }
                }
            }
        }

        private bool Check_Key_Size(string key)
        {
            //Verifie que la longeur de la cl� en param�tre est correcte (16 24 ou 32 caract�res)
            return key.Length == 16 || key.Length == 24 || key.Length == 32;
        }

        private void Serialize(string path, string key)
        {
            //Chiffre et serialise les donn�es dans le dossier mis en param�tre
            if(Check_Key_Size(key))
            {
                DialogResult result = MessageBox.Show("Voulez-vous vraiment enregistrer ? Des donn�es pourraient �tre �cras�es", "Confirmation", MessageBoxButtons.OKCancel);

                // V�rifier la r�ponse de l'utilisateur
                if (result == DialogResult.OK)
                {
                    try
                    {
                        XmlSerializer serializer = new XmlSerializer(typeof(Manager));
                        using (MemoryStream ms = new MemoryStream())
                        {
                            using (TextWriter writer = new StreamWriter(ms))
                            {
                                serializer.Serialize(writer, g);
                            }

                            byte[] encryptedData = Encrypt(Encoding.UTF8.GetString(ms.ToArray()), key);

                            File.WriteAllText(path, Convert.ToBase64String(encryptedData));

                            Display_Console(Color.Green, $"Enregistr� dans {path}");
                        }
                    }
                    catch (Exception ex)
                    {
                        Display_Console(Color.Red, $"Erreur lors de la s�rialisation : {ex.Message}");
                    }
                }
            }
            else
            {
                Display_Console(Color.Red, "Cl� de chiffrement invalide, elle doit avoir une longeur de 16, 24 ou 32 caract�res.");
            }
        }

        private void Deserialize(string path, string key)
        {
            //D�chiffre et d�serialise les donn�es du chemin de fichier mis en param�tre
            if (Check_Key_Size(key))
            {
                DialogResult result = MessageBox.Show("Voulez vous vraiment ouvrir un nouveau gestionnaire ? Les modifications actuelles seront perdues", "Confirmation", MessageBoxButtons.OKCancel);

                // V�rifier la r�ponse de l'utilisateur
                if (result == DialogResult.OK)
                {
                    try
                    {
                        string encryptedData = File.ReadAllText(path);
                        byte[] cipherText = Convert.FromBase64String(encryptedData);
                        string decryptedData = Decrypt(cipherText, key);

                        using (TextReader reader = new StringReader(decryptedData))
                        {
                            XmlSerializer g_serializer = new XmlSerializer(typeof(Manager));
                            Manager? new_g = (Manager?)g_serializer.Deserialize(reader);

                            if (new_g != null)
                            {
                                g = new_g;
                                g.actuel = g.root;

                                // Met � jour les parents car la s�rialisation ne prend pas en compte les cycles parents-enfants
                                g.UpdateParentsRecursive(g.root, null);
                                historique.Clear();
                                positionHistorique = 0;
                                Historique_Add(g.actuel);
                                Update_RedoUndo();
                                Load_Contacts();

                                Display_Console(Color.Green, "Dossier ouvert avec succ�s");
                            }
                            else
                            {
                                Display_Console(Color.Red, "Erreur lors de l'ouverture");
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Display_Console(Color.Red, $"Erreur lors de la d�s�rialisation : {ex.Message}");
                    }
                }
            }
            else
            {
                Display_Console(Color.Red, "Cl� de chiffrement invalide, elle doit avoir une longeur de 16, 24 ou 32 caract�res.");
            }
        }

        private bool Validate_Name(string nom)
        {
            //Verifie si une chaine n'est pas compos�e de "/" ou de "\", ||peut etre supprim�||
            if (nom.Contains("/") || nom.Contains("\\"))
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private void Create_Repository()
        {
            //Ajoute un dossier
            if (g.actuel.existRepository(TBox_rep_nom.Text) != null)
            {
                Display_Console(Color.Red, $"dossier {TBox_rep_nom.Text} d�ja existant");
            }
            else if (Validate_Name(TBox_rep_nom.Text) == false || TBox_rep_nom.Text == "")
            {
                Display_Console(Color.Red, $"nom {TBox_rep_nom.Text} invalide pour un dossier");
            }
            else
            {
                g.mkdir(TBox_rep_nom.Text);
                Load_Contacts();
                Display_Console(Color.Green, $"dossier {TBox_rep_nom.Text} cr�e");
            }
        }

        private void Create_Contact()
        {
            //Ajoute un contact
            if (g.actuel.existContact(TBox_contact_nom.Text, TBox_contact_prenom.Text) != null)
            {
                Display_Console(Color.Red, $"Contact {TBox_contact_nom.Text}.{TBox_contact_prenom.Text} d�ja existant");
            }
            else if (Validate_Name(TBox_contact_nom.Text) == false || TBox_contact_nom.Text == "")
            {
                Display_Console(Color.Red, $"Nom {TBox_contact_nom.Text} invalide pour un contact");
            }
            else if (Validate_Name(TBox_contact_prenom.Text) == false)
            {
                Display_Console(Color.Red, $"Type {TBox_contact_prenom.Text} invalide pour un contact");
            }
            else
            {
                selectedContact = g.touch(TBox_contact_nom.Text, TBox_contact_prenom.Text);
                Load_Contacts();
                Display_Console(Color.Green, $"Contact {TBox_contact_nom.Text}.{TBox_contact_prenom.Text} cr�e");
            }
        }

        private void Remove_Repository(Repository rep)
        {
            //Supprime un dossier
            if (rep.nom != "contacts")
            {
                g.up();

                g.actuel.children.Remove(rep);
                Display_Console(Color.Green, $"Dossier {rep.nom} supprim�");
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Impossible de supprimer le dossier racine");
            }

        }

        private void Remove_Contact(Contact? file)
        {
            //Supprime un contact
            if (file != null && file.parent != null)
            {
                file.parent.children.Remove(file);
                Display_Console(Color.Green, $"Contact {file.nom} {file.prenom} supprim�");
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact selectionn�");
            }
        }

        private void Display_Contact(Contact? contact)
        {
            //Affiche les donn�es d'un contact
            if (contact != null)
            {
                //Affichage du contact
                TBox_setnom.Text = contact.nom;
                TBox_settel.Text = contact.telephone;

                //Afficgage si le contact est un num�ro d'urgence
                if (contact is ContactUrgence)
                {
                    TBox_setprenom.Text = " ";
                    TBox_setsurnom.Text = " ";
                    TBox_setposte.Text = " ";
                }
                else
                {
                    TBox_setprenom.Text = contact.prenom;
                    TBox_setsurnom.Text = contact.surnom;
                    TBox_setposte.Text = contact.poste;
                    TBox_settel.Text = contact.telephone;
                }
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact selectionn�");
            }
        }

        private void Update_RedoUndo()
        {
            //Met a jout les boutons d'historique
            if (positionHistorique == 0)
            {
                BT_redo.Enabled = false;
            }
            else
            {
                BT_redo.Enabled = true;
            }

            if (positionHistorique == historique.Count - 1)
            {
                BT_undo.Enabled = false;
            }
            else
            {
                BT_undo.Enabled = true;
            }

            if (g.actuel == g.root)
            {
                BT_up.Enabled = false;
            }
            else
            {
                BT_up.Enabled = true;
            }
        }

        private void Load_Contacts()
        {
            //Tri des �l�ments en fonction du mode de tri selectionn�
            switch (CBox_tri.Text)
            {
                case "nom":
                    //Tri des �l�ments par ordre alphab�tique de nom (Contact puis dossier)
                    sortedElements = (BT_tri_invert.Text == "\u2193")
                        ? g.actuel.children
                            .OrderByDescending(item => (item is Contact) ? ((Contact)item).nom.ToUpper() : "")
                            .ThenByDescending(item => item.nom.ToUpper())
                            .ToList()
                        : g.actuel.children
                            .OrderBy(item => (item is Contact) ? ((Contact)item).nom.ToUpper() : "")
                            .ThenBy(item => item.nom.ToUpper())
                            .ToList();
                    break;
                case "date":
                    //Tri des �l�ments par date de cr�ation
                    sortedElements = (BT_tri_invert.Text == "\u2193")
                        ? g.actuel.children
                            .OrderByDescending(item => item.creationDate)
                            .ToList()
                        : g.actuel.children
                            .OrderBy(item => item.creationDate)
                            .ToList();
                    break;
                case "prenom":
                    //Tri des �l�ments par ordre alphab�tique de pr�nom (contact puis dossier)
                    sortedElements = (BT_tri_invert.Text == "\u2193")
                        ? g.actuel.children
                            .OrderByDescending(item => (item is Contact) ? ((Contact)item).prenom.ToUpper() : "")
                            .ThenBy(item => item.nom.ToUpper())
                            .ToList()
                        : g.actuel.children
                            .OrderBy(item => (item is Contact) ? ((Contact)item).prenom.ToUpper() : "")
                            .ThenBy(item => item.nom.ToUpper())
                            .ToList();
                    break;
                default:
                    break;
            }

            //Autorisation de supprimer le dossier si il est vide ou si il n'est pas le dossier racine
            if ((sortedElements.Count > 0) || (g.actuel.nom == "contacts"))
            {
                BT_rmdir.Enabled = false;
            }
            else
            {
                BT_rmdir.Enabled= true;
            }

            //Affichage de l'arborescence
            TBox_tree.Text = g.tree();


            //Creation des boutons de selection des dossiers et contacts
            Add_Dynamics_Buttons();

            //Affichage du contact s�lectionn�
            bool enabling = (selectedContact != null);

            if (enabling)
            {
                Display_Contact(selectedContact);
            }

            //Activation / Desactivation des boutons de modification de contact
            BT_copy.Enabled = enabling && !(selectedContact is ContactUrgence);
            BT_update.Enabled = enabling && !(selectedContact is ContactUrgence);
            BT_cut.Enabled = enabling && !(selectedContact is ContactUrgence);
            BT_rm.Enabled = enabling && !(selectedContact is ContactUrgence);
            TBox_setnom.Enabled = enabling && !(selectedContact is ContactUrgence);
            TBox_setprenom.Enabled = enabling && !(selectedContact is ContactUrgence);
            TBox_setposte.Enabled = enabling && !(selectedContact is ContactUrgence);
            TBox_settel.Enabled = enabling && !(selectedContact is ContactUrgence);
            TBox_setsurnom.Enabled = enabling && !(selectedContact is ContactUrgence);
        }

        private void Display_Console(Color color, string texte)
        {
            //Affiche un texte dans la console graphique
            TBox_debug.ForeColor = color;
            TBox_debug.Text = texte;
        }

        private void Select_Repository(Repository rep)
        {
            //Selectionne un dossier en descendant dans l'arborescence
            Display_Console(Color.Green, $"Dossier {rep.nom} ouvert");
            g.down(rep.nom);

            Historique_Add(g.actuel);
            historique.RemoveRange(historique.Count - 1 - positionHistorique, positionHistorique);
            positionHistorique = 0;

            
            Update_RedoUndo();
            Load_Contacts();
        }

        private void Add_Dynamics_Buttons()
        {
            //Creer les boutons de dossier et contact
            Button[] buttons = new Button[sortedElements.Count()];

            flowLayoutPanel.Controls.Clear();
            Panel_Files.Controls.Add(flowLayoutPanel);

            for (int i = 0; i < buttons.Length; i++)
            {
                //Cr�ation des boutons des boutons
                buttons[i] = new Button();
                buttons[i].Size = new Size(btSize, btSize/2);
                buttons[i].Text = sortedElements.ElementAt<Element>(i).nom;

                if (sortedElements.ElementAt<Element>(i) is Contact contact)
                {
                    if (CBox_tri.Text == "nom" || CBox_tri.Text == "date")
                    {
                        buttons[i].Text += $" {contact.prenom}";
                    }
                    else if (CBox_tri.Text == "prenom")
                    {
                        buttons[i].Text = $"{contact.prenom} {contact.nom}";
                    }

                    //Modification de la couleur du bouton si c'est un contact normal, selectionn� ou d'urgence 
                    if (contact == selectedContact)
                    {
                        buttons[i].BackColor = Color.LightGreen;
                    }
                    else if (contact == contactToPaste)
                    {
                        buttons[i].BackColor = Color.Yellow;
                    }
                    else if (contact is ContactUrgence)
                    {
                        buttons[i].BackColor = Color.Salmon;
                    }
                    else
                    {
                        buttons[i].BackColor = Color.LightGray;
                    }
                }

                int currentIndex = i;

                //Evennement si le bouton est cliqu�
                buttons[i].Click += (sender, e) =>
                {

                    if (sortedElements.ElementAt<Element>(currentIndex) is Contact contact)
                    {
                        selectedContact = contact;
                        Display_Console(Color.Green, $"Contact {contact.nom} {contact.prenom} selectionn�");
                        Load_Contacts();
                    }
                    else if (sortedElements.ElementAt<Element>(currentIndex) is Repository folder)
                    {
                        selectedContact = null;
                        Display_Console(Color.Green, $"Dossier {folder.nom} ouvert");
                        Select_Repository(folder);
                    }
                };
                flowLayoutPanel.Controls.Add(buttons[i]);
            }
        }

        private void BT_up_Click(object sender, EventArgs e)
        {
            if (g.up())
            {
                Historique_Add(g.actuel);
                selectedContact = null;
                Display_Console(Color.Green, $"Remont� au dossier {g.actuel.nom}");
                Load_Contacts();
            }
            else
            {
                Display_Console(Color.Red, "D�ja � la racine");
            }
            Update_RedoUndo();
            Load_Contacts();
        }

        private void BT_mkdir_Click(object sender, EventArgs e)
        {
            Create_Repository();
            TBox_rep_nom.Text = null;
        }

        private void BT_touch_Click(object sender, EventArgs e)
        {
            Create_Contact();
            TBox_contact_nom.Text = null;
            TBox_contact_prenom.Text = null;
        }

        private void CBox_tri_SelectedIndexChanged(object sender, EventArgs e)
        {
            Load_Contacts();
        }

        private void BT_tri_invert_Click(object sender, EventArgs e)
        {

            if (BT_tri_invert.Text == "\u2191")
            {
                BT_tri_invert.Text = "\u2193";
            }
            else
            {
                BT_tri_invert.Text = "\u2191";
            }
            Load_Contacts();
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            btSize = trackBar1.Value;
            Add_Dynamics_Buttons();
        }

        private void TBox_mkdir_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Create_Repository();
                TBox_rep_nom.Text = null;
            }
        }

        private void TBox_touch_name_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Create_Contact();
            }
        }

        private void TBox_touch_extension_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Create_Contact();
            }
        }

        private void BT_rmdir_Click(object sender, EventArgs e)
        {
            Remove_Repository(g.actuel);
            Update_RedoUndo();
            Load_Contacts();
        }

        private void BT_rm_Click(object sender, EventArgs e)
        {
            Remove_Contact(selectedContact);
            selectedContact = null;
            Load_Contacts();
        }

        private void Update_Contact(Contact? contact)
        {
            if (contact != null)
            {
                contact.nom = TBox_setnom.Text;
                contact.prenom = TBox_setprenom.Text;
                contact.surnom = TBox_setsurnom.Text;
                contact.poste = TBox_setposte.Text;
                contact.telephone = TBox_settel.Text;

                Display_Console(Color.Green, $"Informations du contact {contact.nom} {contact.prenom} mises � jour");
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact selectionn�");
            }
            Load_Contacts();
        }

        private void BT_update_Click(object sender, EventArgs e)
        {
            Update_Contact(selectedContact);
        }

        private void BT_serialize_Click(object sender, EventArgs e)
        {
            Serialize(TBox_serialize_path.Text, TBox_EncryptKey.Text);
        }

        private void BT_open_Click(object sender, EventArgs e)
        {
            Deserialize(TBox_serialize_path.Text, TBox_EncryptKey.Text);
        }

        private void BT_copy_Click(object sender, EventArgs e)
        {
            contactToPaste = selectedContact;
            if (contactToPaste != null)
            {
                BT_paste.Enabled = true;
                deleteAfterPaste = false;
                Display_Console(Color.Green, $"Contact {contactToPaste.nom} {contactToPaste.prenom} copi�");
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact selectionn�");
            }
        }

        private void BT_cut_Click(object sender, EventArgs e)
        {
            contactToPaste = selectedContact;
            if (contactToPaste != null)
            {
                BT_paste.Enabled = true;
                deleteAfterPaste = true;
                Display_Console(Color.Green, $"Contact {contactToPaste.nom} {contactToPaste.prenom} coup�");
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact selectionn�");
            }

        }

        private void BT_paste_Click(object sender, EventArgs e)
        {
            if (contactToPaste != null)
            {
                string nom = contactToPaste.nom;
                string nom_final = nom;
                string prenom = contactToPaste.prenom;
                int i = 1;

                if (!deleteAfterPaste)
                {
                    Display_Console(Color.Green, $"Contact {nom}.{prenom} copi� ici");
                }
                else
                {
                    Remove_Contact(contactToPaste);
                    Display_Console(Color.Green, $"Contact {nom}.{prenom} coll� ici");
                }
                while (g.actuel.existContact(nom_final, prenom) != null)
                {
                    nom_final = $"{nom}_{i}";
                    i+=1;
                }

                contactToPaste = null;
                BT_paste.Enabled = false;
                selectedContact = g.touch(nom_final, prenom);
                Load_Contacts();
            }
            else
            {
                //Ne doit pas arriver normalement
                Display_Console(Color.Red, "Aucun contact copi� ou coup�");
            }
        }

        private void TBox_setnom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Update_Contact(selectedContact);
            }
        }

        private void TBox_setprenom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Update_Contact(selectedContact);
            }
        }

        private void TBox_setsurnom_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Update_Contact(selectedContact);
            }
        }

        private void TBox_setposte_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Update_Contact(selectedContact);
            }
        }

        private void TBox_settel_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Update_Contact(selectedContact);
            }
        }

        private void TBox_serialize_path_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Deserialize(TBox_serialize_path.Text, TBox_EncryptKey.Text);
            }
        }

        private void BT_redo_Click(object sender, EventArgs e)
        {
            if (positionHistorique == 0)
            {
                //Ne doit pas arriver normalement
                BT_redo.Enabled = false;
                Display_Console(Color.Red, "D�ja � la fin de l'historique");
            }
            else
            {
                selectedContact = null;
                positionHistorique--;
                g.actuel = historique[historique.Count - 1 - positionHistorique];
                Display_Console(Color.Green, $"Remont� au dossier {g.actuel.nom} dans l'historique");
            }
            Update_RedoUndo();
            Load_Contacts();
        }

        private void BT_undo_Click(object sender, EventArgs e)
        {
            if (positionHistorique >= historique.Count - 1)
            {
                BT_undo.Enabled = false;
                Display_Console(Color.Red, "D�ja au d�but de l'historique");
            }
            else
            {
                selectedContact = null;
                positionHistorique++;
                g.actuel = historique[historique.Count - 1 - positionHistorique];
                Display_Console(Color.Green, $"Descendu au dossier {g.actuel.nom} dans l'historique");
            }
            Update_RedoUndo();
            Load_Contacts();
        }
    }
}

