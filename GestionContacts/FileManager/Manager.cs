﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using ContactManager.Tools;

namespace ContactManager
{
    [Serializable]
    [XmlRoot("Manager")]
    [XmlInclude(typeof(Repository))]
    public class Manager
    {
        [XmlElement("contacts")]
        public Repository root { get; set; } = new Repository();

        [XmlElement("actuel")]
        public Repository actuel { get; set; }

        public Manager()
        {
            actuel = root;
            touchUrgence("Samu", "15");
            touchUrgence("Police", "17");
            touchUrgence("Pompiers", "18");
        }

        public void UpdateParentsRecursive(Element element, Repository? parent)
        {
            element.parent = parent;

            if (element is Repository repository)
            {
                foreach (var child in repository.children)
                {
                    UpdateParentsRecursive(child, repository);
                }
            }
        }

        public bool down(string nom)
        {
            Repository? f = actuel.existRepository(nom);
            if (f != null)
            {
                actuel = f;
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool up()
        {
            Repository? rep = actuel.parent;
            if (rep != null)
            {
                actuel = rep;
                return true;
            }
            else
            {
                return false;
            }
        }


        public bool mkdir(string nom)
        {
            if (actuel.existRepository(nom) == null)
            {
                Repository nouveau = new Repository(actuel, nom);
                actuel.children.Add(nouveau);
                return true;
            }
            else
            {
                return false;
            }
        }

        public Contact? touch(string nom, string type)
        {
            if (actuel.existContact(nom, type) == null)
            {
                Contact nouveau = new Contact(actuel, nom, type);
                actuel.children.Add(nouveau);
                return nouveau;
            }
            else
            {
                return null;
            }
            
        }

        public ContactUrgence? touchUrgence(string nom, string tel)
        {
            if (actuel.existContact(nom, "") == null)
            {
                ContactUrgence nouveau = new ContactUrgence(actuel, nom);
                nouveau.telephone = tel;
                actuel.children.Add(nouveau);
                return nouveau;
            }
            else
            {
                return null;
            }
        }

        public string tree()
        {
            StringBuilder treeStringBuilder = new StringBuilder();

            treeStringBuilder.AppendLine(root.nom);
            treeStringBuilder.AppendLine(root.tree("┇", actuel));

            return treeStringBuilder.ToString();
        }

        public override string ToString()
        {
            var cheminBuilder = new StringBuilder();
            Repository? tmp = actuel;

            while (tmp != null)
            {
                cheminBuilder.Insert(0, tmp.nom);

                if (tmp.parent != null)
                {
                    cheminBuilder.Insert(0, System.IO.Path.DirectorySeparatorChar);
                }

                tmp = tmp.parent;
            }

            return cheminBuilder.ToString();
        }
    }
}