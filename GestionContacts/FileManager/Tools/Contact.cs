﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ContactManager.Tools
{
    [Serializable]
    public class Contact : Element
    {
        [XmlElement("prenom")]
        public string prenom { get; set; }

        [XmlElement("surnom")]
        public string surnom { get; set; }

        [XmlElement("poste")]
        public string poste { get; set; }

        [XmlElement("telephone")]
        public string telephone { get; set; }

        public Contact(Repository? parent, string nom, string first) : base(parent, nom)
        {
            prenom = first;
            surnom = "";
            poste = "";
            telephone = "";
        }

        public Contact() : base(null, "contact")
        {
            prenom = "";
            surnom = "";
            poste = "";
            telephone = "";
        }
    }
}