﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ContactManager.Tools
{
    [Serializable]
    public class Element
    {
        [XmlElement("name")]
        public string nom { get; set; }

        [XmlIgnore]
        public Repository? parent { get; set; }

        [XmlElement("date")]
        public DateTime creationDate { get; set; }

        [XmlAttribute("date")]
        public string creationDateString
        {
            get { return creationDate.ToString("yyyy-MM-ddTHH:mm:ss"); }
            set { creationDate = DateTime.Parse(value); }
        }

        public Element(Repository? par, string name)
        {
            nom = name;
            parent = par;
            creationDate = DateTime.Now;
        }

        public Element()
        {
            nom = "";
            parent = null;
            creationDate = DateTime.Now;
        }


        public void setNom(string val)
        {
            nom = val;
        }

        public void setCreationDate(DateTime val)
        {
            creationDate = val;
        }
    }
}

