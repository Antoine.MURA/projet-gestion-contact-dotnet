﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ContactManager.Tools
{
    [Serializable]
    public class ContactUrgence : Contact
    {
        public ContactUrgence(Repository parent, string nom) : base(parent, nom, "")
        {
            surnom = "";
            poste = "";
            telephone = "";
        }

        public ContactUrgence() : base(null, "contact urgence", "")
        {
            surnom = "";
            poste = "";
            telephone = "";
        }
    }
}
