﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ContactManager.Tools
{
    [Serializable]
    public class Repository : Element
    {
        [XmlArray("children")]
        [XmlArrayItem("element", typeof(Element))]
        [XmlArrayItem("contact", typeof(Contact))]
        [XmlArrayItem("contact_urgence", typeof(ContactUrgence))]
        public List<Element> children { get; set; }

        public Repository(Repository? par, string nom) : base(par, nom)
        {
            children = new List<Element>();
        }

        public Repository() : base(null, "contacts")
        {
            children = new List<Element>();
        }

        public string tree(string prefix, Repository actual)
        {
            StringBuilder treeStringBuilder = new StringBuilder();

            foreach (var item in children)
            {
                if (item is Contact contact)
                {
                    treeStringBuilder.AppendLine($"{prefix}  {contact.nom} {contact.prenom}");
                }

                if (item is Repository rep)
                {
                    treeStringBuilder.Append($"{prefix}  ");
                    treeStringBuilder.AppendLine(rep.nom);
                    treeStringBuilder.Append(rep.tree($"{prefix}  ┇", actual));
                    
                }
            }

            return treeStringBuilder.ToString();
        }

        public Repository? existRepository(string nom)
        {
            Repository? folderFound = children.OfType<Repository>().FirstOrDefault(rep => rep.nom == nom);
            return folderFound;
        }

        public Contact? existContact(string nom, string prenom)
        {
            Contact? contactFound = children.OfType<Contact>().FirstOrDefault(contact => contact.nom == nom && contact.prenom == prenom);
            return contactFound;
        }
    }
}